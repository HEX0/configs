#!/bin/sh

# Date
date=$(date "+%a %F %H:%M")

# temp
cpu=$(sensors | grep "Core 0" | cut -f 1-9 -d " ")
gpu=$(sensors | grep "temp1" | cut -f 1-9 -d " ")

# Alsa master volume
volume=$(amixer get Master | grep "%" | cut -f 6-8 -d " ")

# Status bar
echo $volume "|" $cpu "|" $gpu "|" $date
